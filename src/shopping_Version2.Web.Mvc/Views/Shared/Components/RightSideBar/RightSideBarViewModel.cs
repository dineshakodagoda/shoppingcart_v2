﻿using shopping_Version2.Configuration.Ui;

namespace shopping_Version2.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
