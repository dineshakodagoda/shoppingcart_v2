﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace shopping_Version2.Web.Views
{
    public abstract class shopping_Version2RazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected shopping_Version2RazorPage()
        {
            LocalizationSourceName = shopping_Version2Consts.LocalizationSourceName;
        }
    }
}
