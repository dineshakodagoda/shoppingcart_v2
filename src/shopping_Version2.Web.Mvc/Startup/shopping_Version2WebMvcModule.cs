﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using shopping_Version2.Configuration;

namespace shopping_Version2.Web.Startup
{
    [DependsOn(typeof(shopping_Version2WebCoreModule))]
    public class shopping_Version2WebMvcModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public shopping_Version2WebMvcModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<shopping_Version2NavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(shopping_Version2WebMvcModule).GetAssembly());
        }
    }
}
