﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.ObjectMapping;
using Microsoft.AspNetCore.Mvc;
using shopping_Version2.Controllers;
using shopping_Version2.Customers;
using shopping_Version2.Customers.Dto;
using shopping_Version2.Orders;
using shopping_Version2.Orders.Dto;
using shopping_Version2.Products;
using shopping_Version2.Products.Dto;
using shopping_Version2.Web.Models.Order;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace shopping_Version2.Web.Controllers
{

    public class OrderController : shopping_Version2ControllerBase
    {
        private readonly ICustomerService customerservice;
        private readonly IProductService productservice;
        private readonly IOrderService orderservice;
        private readonly IObjectMapper objectmapper;

        public OrderController(ICustomerService customerservice,IProductService productservice
            ,IOrderService orderservice , IObjectMapper objectmapper)
        {
            this.customerservice = customerservice;
            this.productservice = productservice;
            this.orderservice = orderservice;
            this.objectmapper = objectmapper;
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            AddingOrderViewModel model = new AddingOrderViewModel();
            model.CustomerList = objectmapper.Map<List<CustomerDto>>(customerservice.GetAllCustomers());
            model.productList = objectmapper.Map<List<ProductDto>>(productservice.GetAllProducts());
            return View(model);
        }

        [HttpPost]
        
        public IActionResult AddCart([FromBody]OrderViewModel modelsave)
        {
            if (ModelState.IsValid)
            {
                var order = objectmapper.Map<OrderDto>(modelsave);
                orderservice.AddOrder(order);
            }
            else
            {

                TempData["Success"] = false;
            }
            return RedirectToAction("Index", "AddingOrder");
        }

        [HttpGet]
        public IActionResult Orderlist()
        {
            OrderlistViewModel orderlist = new OrderlistViewModel();
            orderlist.orderlist = objectmapper.Map<List<OrderDto>>(orderservice.getAllOrder());
            return View(orderlist);
        }

        [HttpGet]
        public IActionResult OrderDetails(int id)
        {
            var model = objectmapper.Map<OrderDto>(orderservice.getOrderById(id));
            return View(model);
        }

        [HttpGet]
        public IActionResult updateOrder(int id)
        {
            OrderUDto model = new OrderUDto();
            model = objectmapper.Map<OrderUDto>(orderservice.getOrderById(id));
            model.productlist = objectmapper.Map<List<ProductDto>>(productservice.GetAllProducts());
            return View(model);
        }

        [HttpPost]
        public IActionResult saveorder([FromBody]OrderUpdateViewModel orderupdate)
        {
            var updatedorder = objectmapper.Map<OrderUDto>(orderupdate);
            orderservice.updateorder(updatedorder);
            return RedirectToAction("Orderlist", "Order");
        }

        [HttpPost]
        public IActionResult deleteorderline([FromBody]int id)
        {
            orderservice.Deleteorderline(id);
            return Ok();
        }

        public IActionResult deleteOrder(int id)
        {

            orderservice.deleteorder(id);
            return RedirectToAction("Orderlist", "Order");
        }

    }
}
