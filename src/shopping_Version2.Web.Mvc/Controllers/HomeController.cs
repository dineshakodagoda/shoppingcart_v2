﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using shopping_Version2.Controllers;

namespace shopping_Version2.Web.Controllers
{
   
    public class HomeController : shopping_Version2ControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
