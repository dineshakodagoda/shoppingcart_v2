﻿using Abp.AutoMapper;
using shopping_Version2.Roles.Dto;
using shopping_Version2.Web.Models.Common;

namespace shopping_Version2.Web.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class EditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool HasPermission(FlatPermissionDto permission)
        {
            return GrantedPermissionNames.Contains(permission.Name);
        }
    }
}
