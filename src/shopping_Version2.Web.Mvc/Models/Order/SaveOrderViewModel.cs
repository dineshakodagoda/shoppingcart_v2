﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shopping_Version2.Web.Models.Order
{
    public class SaveOrderViewModel
    {

        public int CustomerId { get; set; }

        public DateTime OrderDate { get; set; }
        public List<SaveLineViewModel> orderItems { get; set; }
    }
}
