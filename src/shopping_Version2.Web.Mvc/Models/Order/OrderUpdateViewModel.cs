﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shopping_Version2.Web.Models.Order
{
    public class OrderUpdateViewModel
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public List<UpdateOrderLine> orderItems { get; set; }
        public int customerId { get; set; }
    }
}
