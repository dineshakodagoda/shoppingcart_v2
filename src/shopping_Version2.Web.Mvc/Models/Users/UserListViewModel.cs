using System.Collections.Generic;
using shopping_Version2.Roles.Dto;
using shopping_Version2.Users.Dto;

namespace shopping_Version2.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
