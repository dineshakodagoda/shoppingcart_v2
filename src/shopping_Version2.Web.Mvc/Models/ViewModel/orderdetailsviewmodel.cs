﻿
using shopping_Version2.OrderItems.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingcartProj.ViewModel
{
    public class orderdetailsviewmodel
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        //public int TotalAmount { get; set; }
        public List<OrderItemDto> orderItems { get; set; }
        public int customerId { get; set; }

    }

}
