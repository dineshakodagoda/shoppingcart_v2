﻿
using shopping_Version2.Orders.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingcartProj.ViewModel
{
    public class OrderlistViewModel
    {
        public List<OrderDto> orderlist { get; set; }
    }
}
