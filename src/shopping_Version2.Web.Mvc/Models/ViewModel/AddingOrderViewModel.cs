﻿using shopping_Version2.Customers.Dto;
using shopping_Version2.Products.Dto;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingcartProj.ViewModel
{
    public class AddingOrderViewModel
    {
        public int ProductId { get; set; }
        public List<CustomerDto> CustomerList { get; set; }
        public List<ProductDto> productList { get; set; }
       // public List<OrderItemPL> orderitems { get; set; }

        [Required]
        public int Customerid { get; set; }

        [Required]
        public string ProductName { get; set; }

        public int UnitPrice { get; set; }

        [Required]
        public int Quantity { get; set; }

        public string ProductDescription { get; set; }

        public string OrderDate { get; set; }

      
    }
}
