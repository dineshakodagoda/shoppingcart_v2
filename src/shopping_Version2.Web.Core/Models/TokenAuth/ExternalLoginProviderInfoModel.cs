﻿using Abp.AutoMapper;
using shopping_Version2.Authentication.External;

namespace shopping_Version2.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
