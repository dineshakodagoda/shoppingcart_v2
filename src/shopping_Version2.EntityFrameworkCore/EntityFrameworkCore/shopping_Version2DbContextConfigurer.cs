using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace shopping_Version2.EntityFrameworkCore
{
    public static class shopping_Version2DbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ShoppingDbcontext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ShoppingDbcontext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
