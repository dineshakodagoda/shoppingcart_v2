﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using shopping_Version2.Authorization;

namespace shopping_Version2
{
    [DependsOn(
        typeof(shopping_Version2CoreModule), 
        typeof(AbpAutoMapperModule))]
    public class shopping_Version2ApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<shopping_Version2AuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(shopping_Version2ApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
