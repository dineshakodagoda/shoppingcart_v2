﻿using System.Threading.Tasks;
using shopping_Version2.Configuration.Dto;

namespace shopping_Version2.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
