﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.ObjectMapping;
using Microsoft.EntityFrameworkCore;
using shopping_Version2.OrderItems;
using shopping_Version2.OrderItems.Dto;
using shopping_Version2.Products.Dto;

namespace shopping_Version2.Products
{
    public class ProductService : IProductService
    {
        private readonly IObjectMapper objectmapper;
        private readonly IRepository<Product> productrepository;
        private readonly IRepository<OrderItem> orderitemrepository;
        private readonly IUnitOfWorkManager unitofwork;

        public ProductService( IObjectMapper objectmapper,IRepository<Product> productrepository
            , IRepository<OrderItem> orderitemrepository, IUnitOfWorkManager unitofwork)
        {
            this.objectmapper = objectmapper;
            this.productrepository = productrepository;
            this.orderitemrepository = orderitemrepository;
            this.unitofwork = unitofwork;
        }
        public void addproduct(ProductDto product)
        {
            var productnew = objectmapper.Map<Product>(product);
            productrepository.Insert(productnew);
        }

        public void addqty(List<OrderItemDto> orderitem)
        {
            throw new NotImplementedException();
        }//delete

        public void DeleteUpdateQty(OrderItemDto orderitem)
        {
            throw new NotImplementedException();
        }//delete

        public List<ProductDto> GetAllProducts()
        {
            var products = productrepository.GetAllList();
            var allproducts = objectmapper.Map<List<ProductDto>>(products);
            return allproducts;
        }

        public ProductDto GetProductById(int id)
        {
            var product = productrepository.Get(id);
            var product_ = objectmapper.Map<ProductDto>(product);
            return product_;
        }

        public void UpdateProductQuantity(List<OrderItemDto> orderitem)
        {
            using (var unitOfWork = unitofwork.Begin())
            {
                foreach (var items in orderitem)
                {
                    var existingproduct = productrepository.GetAll().FirstOrDefault(i => i.Id == items.Product.Id);
                    var productqty = existingproduct.Quantity;
                    var existingitem = orderitemrepository.GetAll().AsNoTracking().FirstOrDefault(o => o.Id == items.Id);
                    var existingqty = existingitem.Quantity;
                    var addingqty = items.Quantity;
                    var updateqty = existingqty - addingqty;
                    var newqty = 0;
                    newqty = productqty - (-updateqty);
                    existingproduct.Quantity = newqty;
                    productrepository.Update(existingproduct);
                    unitOfWork.Complete();
                }
            }

        }
    }
}
