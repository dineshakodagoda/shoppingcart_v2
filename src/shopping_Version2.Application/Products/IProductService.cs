﻿using Abp.Application.Services;
using shopping_Version2.OrderItems.Dto;
using shopping_Version2.Products.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Products
{
    public interface IProductService : IApplicationService
    {
        void addproduct(ProductDto porduct);
        List<ProductDto> GetAllProducts();
        ProductDto GetProductById(int id);
        void DeleteUpdateQty(OrderItemDto orderitem);
        void addqty(List<OrderItemDto> orderitem);
        void UpdateProductQuantity(List<OrderItemDto> orderitem);
    }
}
