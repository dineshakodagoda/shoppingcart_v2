﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Products.Dto
{
    public class ProductDto: EntityDto<int>
    {
     
        public string productName { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal price { get; set; }
        public bool availability { get; set; }
    }
}
