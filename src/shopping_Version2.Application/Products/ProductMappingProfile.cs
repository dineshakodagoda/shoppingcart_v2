﻿using AutoMapper;
using shopping_Version2.Products;
using shopping_Version2.Products.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Products
{
   public class ProductMappingProfile : Profile
    {
        public ProductMappingProfile()
        {
            CreateMap<ProductDto, Product>().ReverseMap();
        }
      
    }
}
