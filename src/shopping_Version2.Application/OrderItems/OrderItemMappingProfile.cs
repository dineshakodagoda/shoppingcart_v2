﻿using AutoMapper;
using shopping_Version2.OrderItems;
using shopping_Version2.OrderItems.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.OrderItems
{
   public class OrderItemMappingProfile :Profile
    {
        public OrderItemMappingProfile()
        {
                CreateMap<OrderItemDto, OrderItem>().ReverseMap();
                 CreateMap<OrderItemUDto, OrderItem>().ReverseMap();
        }
    }

}
