﻿using Abp.Application.Services.Dto;
using shopping_Version2.Orders.Dto;
using shopping_Version2.Products.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace shopping_Version2.OrderItems.Dto
{
   public class OrderItemDto: EntityDto<int>
    {
      
        public int productid { get; set; }

        [ForeignKey(nameof(productid))]
        public virtual ProductDto Product { get; set; }
        public int OrderId { get; set; }

        [ForeignKey(nameof(OrderId))]
        public virtual OrderDto Order { get; set; }

        [Required]
        [Range(maximum: 10, minimum: 0, ErrorMessage = "Maximum amount is 10")]
        public int Quantity { get; set; }
        public int UnitPrice { get; set; }
        public bool isDeleted { get; set; }
    }
}
