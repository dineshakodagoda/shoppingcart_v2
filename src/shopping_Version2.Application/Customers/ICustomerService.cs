﻿using Abp.Application.Services;
using shopping_Version2.Customers.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Customers
{
    public interface ICustomerService : IApplicationService
    {
        List<CustomerDto> GetAllCustomers();
       CustomerDto GetCustomerById(int id);
    }
}
