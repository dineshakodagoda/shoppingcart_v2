﻿using AutoMapper;

using shopping_Version2.Customers;
using shopping_Version2.Customers.Dto;
using shopping_Version2.Products.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Customers
{
   public class CustomerMappingProfile:Profile
    {
        public CustomerMappingProfile()
        {
            CreateMap<CustomerDto,Customer>().ReverseMap();

        }
    }
}
