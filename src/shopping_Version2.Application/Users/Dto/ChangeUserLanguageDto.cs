using System.ComponentModel.DataAnnotations;

namespace shopping_Version2.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}