using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using shopping_Version2.Roles.Dto;
using shopping_Version2.Users.Dto;

namespace shopping_Version2.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
