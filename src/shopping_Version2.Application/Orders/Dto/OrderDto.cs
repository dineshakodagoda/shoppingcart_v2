﻿using Abp.Application.Services.Dto;
using shopping_Version2.Customers.Dto;
using shopping_Version2.OrderItems.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace shopping_Version2.Orders.Dto
{
   public class OrderDto: EntityDto<int>
    {
       
        [Required]
        public DateTime OrderDate { get; set; }
        [Required]
        public List<OrderItemDto> orderItems { get; set; }

        [Required(ErrorMessage = "select a customer")]
        public int customerId { get; set; }
        [ForeignKey(nameof(customerId))]
        public virtual CustomerDto customer { get; set; }
    }
}
