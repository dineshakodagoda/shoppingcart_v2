﻿using Abp.Application.Services.Dto;
using shopping_Version2.Customers.Dto;
using shopping_Version2.OrderItems.Dto;
using shopping_Version2.Products.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace shopping_Version2.Orders.Dto
{
   public class OrderUDto : EntityDto<int>
    {
       
        [Required]
        public DateTime OrderDate { get; set; }
        [Required]
        public List<OrderItemUDto> orderItems { get; set; }

        [Required(ErrorMessage = "select a customer")]
        public int customerId { get; set; }
        [ForeignKey(nameof(customerId))]
        public virtual CustomerDto customer { get; set; }
        public List<ProductDto> productlist { get; set; }
    }
}
