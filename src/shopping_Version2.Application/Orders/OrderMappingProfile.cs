﻿using AutoMapper;
using shopping_Version2.Orders.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Orders
{
   public class OrderMappingProfile :Profile
    {
        public OrderMappingProfile()
        {
            CreateMap<OrderDto, Order>().ReverseMap();
            CreateMap<OrderUDto, Order>().ReverseMap();
           
        }
    }
}
