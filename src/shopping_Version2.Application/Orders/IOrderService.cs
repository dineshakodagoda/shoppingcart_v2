﻿using Abp.Application.Services;
using shopping_Version2.Orders.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Orders
{
   public interface IOrderService : IApplicationService
    {
        void AddOrder(OrderDto order);
        List<OrderDto> getAllOrder();
        OrderDto getOrderById(int id);
        void updateorder(OrderUDto orderdto);
        void Deleteorderline(int id);
        void deleteorder(int id);
    }
 }
