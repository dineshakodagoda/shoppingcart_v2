﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using shopping_Version2.MultiTenancy.Dto;

namespace shopping_Version2.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

