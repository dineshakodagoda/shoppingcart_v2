﻿using System.Threading.Tasks;
using Abp.Application.Services;
using shopping_Version2.Sessions.Dto;

namespace shopping_Version2.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
