﻿namespace shopping_Version2.Roles.Dto
{
    public class GetRolesInput
    {
        public string Permission { get; set; }
    }
}
