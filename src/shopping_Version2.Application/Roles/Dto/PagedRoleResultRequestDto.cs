﻿using Abp.Application.Services.Dto;

namespace shopping_Version2.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

