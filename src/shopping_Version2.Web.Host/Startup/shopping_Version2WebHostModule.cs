﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using shopping_Version2.Configuration;

namespace shopping_Version2.Web.Host.Startup
{
    [DependsOn(
       typeof(shopping_Version2WebCoreModule))]
    public class shopping_Version2WebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public shopping_Version2WebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(shopping_Version2WebHostModule).GetAssembly());
        }
    }
}
