﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Products
{
    public class Product : Entity<int>
    {
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }

    }
}
