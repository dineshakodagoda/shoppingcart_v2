﻿using Abp.Domain.Entities;
using shopping_Version2.Orders;
using shopping_Version2.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace shopping_Version2.OrderItems
{
    public class OrderItem : Entity<int>
    {
      
        public int productid { get; set; }
        [ForeignKey(nameof(productid))]
        public virtual Product product { get; set; }
        public int orderid { get; set; }

        [ForeignKey(nameof(orderid))]
        public virtual Order order { get; set; }
        [Required]
        [Range(maximum: 10, minimum: 0, ErrorMessage = "Maximum amount is 10")]
        public int Quantity { get; set; }
        public int UnitPrice { get; set; }

    }
}
