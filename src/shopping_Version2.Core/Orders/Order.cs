﻿using Abp.Domain.Entities;
using shopping_Version2.Customers;
using shopping_Version2.OrderItems;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace shopping_Version2.Orders
{
    public class Order : Entity<int>
    {
       
        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        public List<OrderItem> orderItems { get; set; }

        [Required(ErrorMessage = "select a customer")]
        public int customerId { get; set; }

        [ForeignKey(nameof(customerId))]
        public virtual Customer customer { get; set; }
    }
}
