﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace shopping_Version2.Localization
{
    public static class shopping_Version2LocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(shopping_Version2Consts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(shopping_Version2LocalizationConfigurer).GetAssembly(),
                        "shopping_Version2.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
