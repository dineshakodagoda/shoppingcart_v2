﻿using Abp.Authorization;
using shopping_Version2.Authorization.Roles;
using shopping_Version2.Authorization.Users;

namespace shopping_Version2.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
